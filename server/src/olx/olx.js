const puppeteer = require('puppeteer');
const fs = require('fs');
const { throws } = require('assert');

async function getEstados() {
    const path = __dirname + '/json/estados.json';
    let estados;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        const url = "https://www.olx.com.br/";
        await page.goto(`${url}`);

        estados = await page.evaluate(() => {
            let estadosList = document.querySelectorAll('div[class="container"] a');
            let estados = [];
            estadosList.forEach((e) => {
                estados.push({ uf: e.text.toLowerCase(), url: e.getAttribute('href') })
            });
            return estados;
        });

        await browser.close();

        estados = estados.sort(function (a, b) {
            return a.uf < b.uf ? -1 : a.uf > b.uf ? 1 : 0;
        });

        estados = estados.filter(a => a.uf !== 'brasil');

        // fs.writeFile(path, JSON.stringify(estados), err => {
        //     if (err) {
        //         console.error(err)
        //         return
        //     }
        // });
        return estados;
    }
}

async function getRegioes(url, uf) {
    url = decodeURIComponent(url);

    const path = `${__dirname}/json/regioes-${uf}.json`;
    let regioes;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        await page.goto(`${url}`);

        regioes = await page.evaluate(() => {
            let regioesList = document.querySelectorAll('a[data-lurker-detail="linkshelf_item"]');
            let regioes = [];
            regioesList.forEach((e) => {
                regioes.push({ nome: e.text, url: e.getAttribute('href') })
            });
            return regioes;
        });

        await browser.close();

        // fs.writeFile(path, JSON.stringify(regioes), err => {
        //     if (err) {
        //         console.error(err)
        //         return
        //     }
        // });
        return regioes;
    }
}

async function getCidades(url) {
    url = decodeURIComponent(url);

    let b = url.split('/');
    b = b[b.length - 1];

    const path = `${__dirname}/json/cidades-${b}.json`;
    let cidades;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        await page.goto(`${url}`);

        cidades = await page.evaluate(() => {
            let cidadesList = document.querySelectorAll('a[data-lurker-detail="linkshelf_item"]');
            let cidades = [];
            cidadesList.forEach((e) => {
                cidades.push({ nome: e.text, url: e.getAttribute('href') })
            });
            return cidades;
        });

        await browser.close();

        // fs.writeFile(path, JSON.stringify(bairros), err => {
        //     if (err) {
        //         console.error(err)
        //         return
        //     }
        // });
        return cidades;
    }
}

async function getBairros(url) {
    url = decodeURIComponent(url);

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 900, height: 600 });

    await page.goto(`${url}`);

    page.hover('li span[font-weight=bold]');

    await page.waitForFunction("document.querySelector('span[data-testid]')");

    await page.click('span[data-testid]');

    await page.waitForFunction("document.querySelector('div[class^=\"ReactModal__Content\"]')");

    let bairros = await page.evaluate(() => {
        let bairros = [];
        let bairrosList = document.querySelectorAll('label[for^="location"]');
        bairrosList.forEach((b) => {
            if (b.textContent.split(',')[1].replace(' ', '') > 0) {
                let nome = b.textContent.split(',')[0];
                let codUrl = b.getAttribute('for').replace('location-', '');
                bairros.push({ nome: nome, codUrl: codUrl })
            }
        });

        return bairros;
    });

    await browser.close();
    return bairros;
}

async function getCategorias() {
    const path = __dirname + '/json/categorias.json';
    let categorias;
    if (fs.existsSync(path)) {

        return fs.readFileSync(path, 'utf8')

    } else {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setViewport({ width: 900, height: 600 });

        const url = "https://www.olx.com.br/brasil";
        await page.goto(`${url}`);

        categorias = await page.evaluate(() => {
            let categoriasList = document.querySelectorAll('[class="sticky-inner-wrapper"] div div div div a');
            let categorias = [];
            categoriasList.forEach((e) => {
                let eArr = e.getAttribute('href').split('/');
                categorias.push({ nome: e.textContent, partUrl: eArr[eArr.length - 1] })
            });
            return categorias;
        });

        await browser.close();

        // fs.writeFile(path, JSON.stringify(categorias), err => {
        //     if (err) {
        //         console.error(err)
        //         return
        //     }
        // });
        return categorias;
    }
}

async function getCategorias2(url) {
    url = decodeURIComponent(url);

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 900, height: 600 });

    await page.goto(`${url}`);

    let categorias2;
    if (await page.$('a[data-lurker-detail="search_subcategory"]')) {
        categorias2 = await page.evaluate(() => {
            let catList = document.querySelectorAll('a[data-lurker-detail="search_subcategory"]');
            let cat = [];
            catList.forEach((e) => {
                let eArr = e.getAttribute('href').split('/');
                cat.push({ nome: e.text.split('(')[0], partUrl: eArr[eArr.length - 1] })
            });
            return cat;
        });
    }

    await browser.close();

    return categorias2 ? categorias2 : [];
}

async function getInfoPages(url) {
    url = decodeURIComponent(url);

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 900, height: 600 });

    await page.goto(`${url}`);

    let infoPages;
    if (await page.$('a[href][data-lurker-detail="last_page"]')) {
        infoPages = await page.evaluate(() => {
            let element = document.querySelector('div[id="column-main-content"] span[color="grayscale.darker"][class][font-weight]');
            let qtdeAnuncios = element.textContent.split(' ')[4].replace('.', '');
            if (qtdeAnuncios > 5000) {
                qtdeAnuncios = 5000;
            }
            let qtdePages = Math.ceil(qtdeAnuncios / 50);
            return { qtdePages: qtdePages, qtdeAnuncios: parseInt(qtdeAnuncios) };
        });
    }

    await browser.close();

    return infoPages;
}

async function getUrlsAnuncios(url) {
    url = decodeURIComponent(url);

    let urlsAnuncios;
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 900, height: 600 });

    await page.goto(`${url}`);

    urlsAnuncios = await page.evaluate(() => {
        let urlsAnunciosList = document.querySelectorAll('ul[id="ad-list"] li a[data-lurker-detail="list_id"]');
        let urlsAnuncios = [];
        urlsAnunciosList.forEach((e) => {
            urlsAnuncios.push({ url: e.getAttribute('href') })
        });
        return urlsAnuncios;
    });

    await browser.close();

    return urlsAnuncios;
}

async function getPhones(urls) {
    const launchOptions = {
        headless: true
        // , args: ["--start-maximized"]
        // , args: ["--user-data-dir=./Google/Chrome/User Data/"]
    };
    const browser = await puppeteer.launch(launchOptions);
    const page = await browser.newPage();
    await page.setViewport({ width: 1000, height: 600 });

    let phones = [];
    let dados = [];

    for (let i = 0; i < urls.length; i++) {
        await page.goto(urls[i].url);

        // ESPERA O CARREGAMENTO
        await page.waitForSelector('[id="miniprofile"] div[color="#f9f9f9"][bordercolor="#d8d8d8"] span[color="dark"]');

        // CLICA EM MAIS INFORMAÇÕES (SE HOUVER)
        // const textMaisInfo = await page.$eval('div a[href="javascript:void(0)"]', element => element.innerHTML);
        // if (textMaisInfo.includes("completa")) {
        if (await page.$('div p[style*="position"] a[href]')) {
            await page.click('div p[style*="position"] a[href]');
        }

        // CLICA EM VER NUMERO DO ANÚNCIO (SE HOUVER)
        while (await page.$('p span[class][color="dark"][font-weight] a')) {
            await page.click('p span[class][color="dark"][font-weight] a')
        }
        // if (page.waitForSelector('p span[class][color="dark"][font-weight] a')) {
        //     await page.click('p span[class][color="dark"][font-weight] a')
        // }


        // // CLICA EM VER NUMERO DO PERFIL (SE HOUVER)
        // if (page.waitForSelector('[id="miniprofile"] a span[href]')) {
        //     await page.click('[id="miniprofile"] a span[href]');
        //     await tempoEspera(1000);
        // }


        phones.push(await page.evaluate(async () => {
            // Pegar descrição do anuncio
            let descricaoAnuncio = document.querySelector('p span[class][color="dark"][font-weight]').textContent

            // Filtrar números de celulares
            let rx = [];
            rx.push(/\(?\[?[\d{1}]? ?[\d{1}]? ?[\d{1}]? ?[\d{1}]? ?\)?\]?\/? ?[\d{1}]? ?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1} ?\-?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1}/g);
            rx.push(/\(?\[?[1-9]{1} ?\d{1}\)?\]?? ?[\d{1}]? ?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1} ?\-?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1} ?\.? ?\d{1}/g);

            let phoneNumber = [];

            // // BUSCAR NUMERO DO PERFIL
            // $elemTelPrincipal = document.querySelectorAll('[id="miniprofile"] a[href="#"][type="secondary"][font-weight] div')[1];
            // if ($elemTelPrincipal) {
            //     console.log($elemTelPrincipal.textContent)
            //     phoneNumber.push($elemTelPrincipal.textContent);
            // }

            rx.forEach(async (element) => {
                let phoneArray = [];
                while (phoneArray = element.exec(descricaoAnuncio)) {
                    if (phoneArray && !phoneNumber.includes(phoneArray[0].replace(/\D/gim, ''))) {
                        phoneNumber.push(phoneArray[0].replace(/\D/gim, ''));
                    }
                }
            })

            // Reunir dados para retorno
            let dados;
            if (phoneNumber.length > 0) {
                for (let index = 0; index < phoneNumber.length; index++) {
                    // Set nome do anunciante
                    let nomeAnunciante = document.querySelector('[id="miniprofile"] div[color="#f9f9f9"][bordercolor="#d8d8d8"] div div span[color="dark"][font-weight]').textContent;

                    // Set categorias do anuncio
                    let categList = document.querySelectorAll('a[color="dark"][class][type="secondary"][font-weight]');

                    categList = Array.from(categList).map(e => e.text);

                    dados = { url: document.URL, nome: nomeAnunciante, categorias: categList, telefones: phoneNumber }
                }
            }

            return dados;

        }));

    }

    await browser.close();

    phones = await removeDuplicados(phones);

    dados = await formatResultado(phones);

    // const path = __dirname + '/json/resultado.json';
    // fs.appendFile(path, JSON.stringify(dados), function (err) {
    //     if (err) throw err;
    // });

    return dados;
}

async function formatResultado(phones) {
    let dados = [];
    for (let i = 0; i < phones.length; i++) {
        for (let j = 0; j < phones[i].telefones.length; j++) {
            let nome = phones[i].nome;
            let url = phones[i].url;
            let categorias = phones[i].categorias;
            let telefone = phones[i].telefones[j]
            dados.push({ nome, url, categorias, telefone });
        }
    }
    return dados;
}
async function removeDuplicados(phones) {
    // REMOVER TELEFONES DUPLICADOS
    let indexPrincipalRemove = [];
    for (let k = 0; k < phones.length; k++) {
        if (!!phones[k]) {

            let telefones = phones[k].telefones;

            for (let w = 0; w < telefones.length; w++) {
                let indexRemove = [];
                for (let y = 0; y < telefones.length; y++) {
                    if (telefones[w] !== telefones[y]) {
                        if (telefones[w].includes(telefones[y].substr(1)) || telefones[y].includes('000')) {
                            indexRemove.push(y);
                        }
                        if (telefones[y].includes(telefones[w].substr(1)) || telefones[w].includes('000')) {
                            indexRemove.push(w);
                        }
                    }
                }
                if (indexRemove.length > 0) {
                    indexRemove.forEach(i => {
                        phones[k].telefones.splice(i, 1);
                    });
                }
            }
        } else {
            indexPrincipalRemove.push(k);
            phones.splice(k, 1);
        }
    }
    if (indexPrincipalRemove.length > 0) {
        indexPrincipalRemove.forEach(i => {
            phones.splice(i, 1);
        });
    }
    return phones;
}

async function tempoEspera(duration) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration)
    });
}

module.exports = {
    getEstados,
    getRegioes,
    getCidades,
    getBairros,
    getCategorias,
    getCategorias2,
    getInfoPages,
    getUrlsAnuncios,
    getPhones
};