const express = require('express')
const puppeteer = require('puppeteer')
const olx = require('../olx/olx')

const router = express.Router()

router.get('/olx-api/get-estados', async (req, res) => {
    const data = await olx.getEstados()
    res.send(data)
})

router.get('/olx-api/get-regioes', async (req, res) => {
    const data = await olx.getRegioes(req.query.url, req.query.uf)
    res.send(data)
})

router.get('/olx-api/get-cidades', async (req, res) => {
    const data = await olx.getCidades(req.query.url)
    res.send(data)
})

router.get('/olx-api/get-bairros', async (req, res) => {
    const data = await olx.getBairros(req.query.url)
    res.send(data)
})

router.get('/olx-api/get-categorias', async (req, res) => {
    const data = await olx.getCategorias()
    res.send(data)
})

router.get('/olx-api/get-categorias-2', async (req, res) => {
    const data = await olx.getCategorias2(req.query.url)
    res.send(data)
})

router.get('/olx-api/get-info-pages', async (req, res) => {
    const data = await olx.getInfoPages(req.query.url)
    res.send(data)
})

router.get('/olx-api/get-urls-anuncios', async (req, res) => {
    const data = await olx.getUrlsAnuncios(req.query.url)
    res.send(data)
})

router.post('/olx-api/list-phones', async (req, res) => {
    try {
        const data = await olx.getPhones(req.body)
        res.send(data)
    } catch (error) {
        res.status(404).send(error);
    }
})

module.exports = app => app.use('', router)