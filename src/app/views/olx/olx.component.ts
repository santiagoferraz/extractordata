import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { OlxService } from '../../services/olx.service';
import { NgModel } from '@angular/forms';
import { error } from '@angular/compiler/src/util';

const qtdeVisitasPorRequest = 3;

@Component({
  selector: 'app-olx',
  templateUrl: './olx.component.html',
  styleUrls: ['./olx.component.scss']
})
export class OlxComponent implements OnInit {
  palavraChave: string = '';
  estados: any[] = [];
  regioes: any[] = [];
  cidades: any[] = [];
  bairros: any[] = [];
  categorias: any[] = [];
  categorias2: any[] = [];
  selectedEstado: string = '';
  selectedRegiao: string = '';
  selectedCidade: string = '';
  selectedBairro: any[] = [];
  selectedCategoria: string = '';
  selectedCategoria2: string = '';
  exibirProgressBar: boolean = false;
  urlsPesquisa: any[] = [];
  resultadoDados: any[] = [];
  enumNumberOfages: any[] = [];
  qtdeAnuncios: number = 0;
  qtdeVisitadas: number = 0;
  stopBusca: boolean = false;

  displayedColumns: string[] = ['nome', 'url', 'categorias', 'telefone'];

  constructor(
    private OlxService: OlxService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.resetCategoria2();
    this.resetRegioes();
    this.resetCidades();
    this.resetBairros();
    this.getEstados();
    this.getCategorias();
  }

  selectUnselectAll(select: NgModel, values: any[]) {
    if (this.selectedBairro.length > 1) {
      select.update.emit([]);
    } else {
      select.update.emit(values);
    }
  }

  getDados() {
    this.stopBusca = false;
    this.exibirProgressBar = true;
    let urlBase: string = '';
    let urlCategoria: string = '';
    let urlCategoria2: string = '';
    let urlBairro: string = '';

    let cidade = this.cidades.filter(a => a.url === this.selectedCidade)[0];
    if (cidade.url === 'todos') {
      let regiao = this.regioes.filter(a => a.url === this.selectedRegiao)[0];
      if (regiao.url === 'todos') {
        let estado = this.estados.filter(a => a.uf === this.selectedEstado)[0];
        urlBase = estado.url;
      } else {
        urlBase = regiao.url;
      }
    } else {
      urlBase = cidade.url;
    }

    if (this.selectedCategoria !== 'todos') {
      let categoria = this.categorias.filter(a => a.partUrl === this.selectedCategoria)[0];
      urlCategoria = categoria.partUrl;
    }
    
    if (this.selectedCategoria2 !== 'todos') {
      let categoria2 = this.categorias2.filter(a => a.partUrl === this.selectedCategoria2)[0];
      urlCategoria2 = categoria2.partUrl;
    }

    if (this.selectedBairro && this.selectedBairro.length > 0) {
      urlBairro = this.selectedBairro.map((b: any) => 'sd=' + b.codUrl).join('&');
    }

    let urlBusca = this.createUrlToSearch(urlBase, urlCategoria, urlCategoria2, 0, this.palavraChave, urlBairro);

    this.OlxService.getInfoPages(urlBusca).subscribe((infoPages) => {
      this.qtdeAnuncios = infoPages.qtdeAnuncios;

      // CRIA ENUM DE PAGINAS PARA RANDOM E NÃO REPETIR
      for (let k = 0; k < infoPages.qtdePages; k++) {
        this.enumNumberOfages.push(k);
      }

      this.prepararEnvio(urlBase, urlCategoria, urlCategoria2, urlBairro);
    })

  }

  prepararEnvio(urlBase: string, urlCategoria: string, urlCategoria2: string, urlBairro: string) {
    let numberPage = this.generateRandom();
    let urlBusca = this.createUrlToSearch(urlBase, urlCategoria, urlCategoria2, numberPage, this.palavraChave, urlBairro);

    this.OlxService.getUrlsPesquisa(urlBusca).subscribe((urls) => {
      this.urlsPesquisa = [...urls];
      this.requestDados(urlBase, urlCategoria, urlCategoria2, urlBairro);
    })
  }

  createUrlToSearch(urlPrincipal: string, urlCategoria: string, urlCategoria2: string, numberPage: number, palavraChave: string, bairros: string) {
    let url = '';
    let urlPart = [];

    url = urlPrincipal;
    if (urlCategoria) {
      url += '/' + urlCategoria;
    }
    if (urlCategoria2) {
      url += '/' + urlCategoria2;
    }
    if (numberPage >= 0) {
      urlPart.push('o=' + numberPage);
    }
    if (palavraChave) {
      urlPart.push('q=' + palavraChave);
    }
    if (bairros) {
      urlPart.push(bairros);
    }
    if (urlPart.length > 0) {
      url += '?' + urlPart.join('&');
    }
    return url;
  }

  async requestDados(urlBase: string, urlCategoria: string, urlCategoria2: string, urlBairro: string) {
    let urls: any = await this.prepareUrlsSend(this.urlsPesquisa);
    if ((this.qtdeVisitadas + qtdeVisitasPorRequest) > this.qtdeAnuncios) {
      this.qtdeVisitadas = this.qtdeAnuncios;
    } else {
      this.qtdeVisitadas += qtdeVisitasPorRequest;
    }
    this.OlxService.extrairDados(urls).subscribe((data) => {
      let res = [...data];

      this.resultadoDados = this.resultadoDados.concat(res.filter((item1) => {
        return this.resultadoDados.map((item2) => {
          return item2.telefone;
        }).indexOf(item1.telefone) < 0
      }));

      if (!this.stopBusca && this.qtdeVisitadas < this.qtdeAnuncios) {
        this.enviarNovamente(urlBase, urlCategoria, urlCategoria2, urlBairro);
      } else {
        this.finalizar();
      }
      
    }, (error) => {
      if (!this.stopBusca && this.qtdeVisitadas < this.qtdeAnuncios) {
        this.enviarNovamente(urlBase, urlCategoria, urlCategoria2, urlBairro);
      } else {
        this.finalizar();
      }
    })
  }

  finalizar() {
    this.stopBusca = true;
  }

  enviarNovamente(urlBase: string, urlCategoria: string, urlCategoria2: string, urlBairro: string) {
    if (this.urlsPesquisa.length > 0) {
      this.requestDados(urlBase, urlCategoria, urlCategoria2, urlBairro);
    } else if (this.enumNumberOfages.length > 0) {
      this.prepararEnvio(urlBase, urlCategoria, urlCategoria2, urlBairro);
    }
  }

  prepareUrlsSend(urls: any) {
    return new Promise(function (resolve) {
      let dataSend: any[] = [];
      for (let i = 0; i < qtdeVisitasPorRequest; i++) {
        if (urls[i]) {
          dataSend.push(urls[i]);
          urls.shift();
        }
      }
      resolve(dataSend);
    });
  }

  generateRandom() {
    let index = Math.floor(Math.random() * (this.enumNumberOfages.length - 0 + 1)) + 0;
    let numberPage = this.enumNumberOfages[index];
    this.enumNumberOfages.splice(index, 1);
    return numberPage;
  }

  getEstados() {
    this.spinner.show();
    this.OlxService.getEstados().subscribe((data) => {
      this.estados = data;
      this.estados.unshift({ uf: 'TODOS', url: 'https://www.olx.com.br/brasil' });
      this.selectedEstado = this.estados[0].uf;
      this.spinner.hide();
    })
  }

  getCategorias() {
    this.spinner.show();
    this.OlxService.getCategorias().subscribe((data) => {
      this.categorias = data;
      this.categorias.unshift({ nome: 'TODOS', partUrl: 'todos' });
      this.selectedCategoria = this.categorias[0].partUrl;
      this.spinner.hide();
    })
  }

  eventChangeEstado(event: any) {
    this.resetRegioes();
    this.resetCidades();
    this.resetBairros();

    if (event.value !== 'TODOS') {
      this.spinner.show();
      event = this.estados.filter(a => a.uf === event.value);
      let url = event[0].url;
      let uf = event[0].uf;
      this.OlxService.getRegioes(url, uf).subscribe((data) => {
        this.regioes = data;
        this.regioes.unshift({ nome: 'TODOS', url: 'todos' });
        this.selectedRegiao = this.regioes[0].url;
        this.spinner.hide();
      })
    }
  }

  eventChangeCategoria(event: any) {
    this.resetCategoria2();
    if (event.value !== 'todos') {
      this.spinner.show();
      let url = `https://www.olx.com.br/${event.value}`;
      this.OlxService.getCategorias2(url).subscribe((data) => {
        this.categorias2 = data;
        this.categorias2.unshift({ nome: 'TODOS', partUrl: 'todos' });
        this.selectedCategoria2 = this.categorias2[0].partUrl;
        this.spinner.hide();
      })
    }
  }

  eventChangeRegiao(event: any) {
    this.resetCidades();
    this.resetBairros();
    if (event.value !== 'todos') {
      this.spinner.show();
      this.OlxService.getCidades(event.value).subscribe((data) => {
        this.cidades = data;
        this.cidades.unshift({ nome: 'TODOS', url: 'todos' });
        this.selectedCidade = this.cidades[0].url;

        if (data.length === 1) {
          this.getBairros(event.value);
        } else {
          this.spinner.hide();
        }

      })
    }
  }

  eventChangeCidade(event: any) {
    this.resetBairros();
    if (event.value !== 'todos') {
      this.spinner.show();
      this.getBairros(event.value);
    }
  }

  getBairros(url: string) {
    this.OlxService.getBairros(url).subscribe((data) => {
      this.bairros = data;
      this.spinner.hide();
    })
  }

  private resetCategoria2() {
    this.categorias2 = [{ nome: 'TODOS', partUrl: 'todos' }];
    this.selectedCategoria2 = this.categorias2[0].partUrl;
  }
  private resetRegioes() {
    this.regioes = [{ nome: 'TODOS', url: 'todos' }];
    this.selectedRegiao = this.regioes[0].url;
  }
  private resetCidades() {
    this.cidades = [{ nome: 'TODOS', url: 'todos' }];
    this.selectedCidade = this.cidades[0].url;
  }
  private resetBairros() {
    this.bairros = [];
    this.selectedBairro = this.bairros[0];
  }
  private resetVariaveis() {
    this.palavraChave = '';
    this.estados = [];
    this.regioes = [];
    this.cidades = [];
    this.bairros = [];
    this.categorias = [];
    this.selectedEstado = '';
    this.selectedRegiao = '';
    this.selectedCidade = '';
    this.selectedBairro = [];
    this.selectedCategoria = '';
    this.exibirProgressBar = false;
    this.urlsPesquisa = [];
    this.resultadoDados = [];
    this.enumNumberOfages = [];
    this.qtdeAnuncios = 0;
    this.qtdeVisitadas = 0;
    this.stopBusca = false;
  }


  stopSearch() {
    this.stopBusca = true;
  }

  resetAll() {
    this.resetVariaveis();
    this.resetRegioes();
    this.resetCidades();
    this.resetBairros();
    this.getEstados();
    this.getCategorias();
  }

  printComponent() {
    window.print();
  }

}
