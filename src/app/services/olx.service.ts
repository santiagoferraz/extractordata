import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const host = '127.0.0.1';
const port = '3003';


@Injectable({
  providedIn: 'root'
})
export class OlxService {

  apiUrl = `http://${host}:${port}/olx-api/`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'aplication/json'
    })
  }

  constructor(
    private httpClient: HttpClient
  ) { }

  public getEstados(): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}get-estados`);
  }
  public getRegioes(url: string, uf: string): Observable<any> {
    url = encodeURIComponent(url);
    return this.httpClient.get<any>(`${this.apiUrl}get-regioes?url=${url}&uf=${uf}`);
  }
  public getCidades(url: string): Observable<any> {
    url = encodeURIComponent(url);
    return this.httpClient.get<any>(`${this.apiUrl}get-cidades?url=${url}`);
  }
  public getBairros(url: string): Observable<any> {
    url = encodeURIComponent(url);
    return this.httpClient.get<any>(`${this.apiUrl}get-bairros?url=${url}`);
  }
  public getCategorias(): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}get-categorias`);
  }
  public getCategorias2(url: string): Observable<any> {
    return this.httpClient.get<any>(`${this.apiUrl}get-categorias-2?url=${url}`);
  }
  public getInfoPages(url: string): Observable<any> {
    url = encodeURIComponent(url);
    return this.httpClient.get<any>(`${this.apiUrl}get-info-pages?url=${url}`);
  }
  public getUrlsPesquisa(url: string): Observable<any> {
    url = encodeURIComponent(url);
    return this.httpClient.get<any>(`${this.apiUrl}get-urls-anuncios?url=${url}`);
  }
  public extrairDados(urls: any[]): Observable<any> {
    return this.httpClient.post(`${this.apiUrl}list-phones`, urls).pipe();
  }
}
